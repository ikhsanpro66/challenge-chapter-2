const readline = require('readline');

const Nilaiinput = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function input(question) {
    return new Promise(resolve => {
        Nilaiinput.question(question, (data) => {
            resolve(data);
        });
    });
}
let array = [];

function showResult() {
    console.clear();
    console.log('Nilai yang telah anda simpan adalah : \n', array.join(", "))
    showMinMax();
    showAvg(array);
    showPass(array);
    showSorted(array);
    showHigh(array)
}

function pushArray(y) {
    if (y != "q") {
        array.push(+y);
        console.clear();
        console.log(`Kamu menambahkan ${y} , Nilai saat ini adalah : \n `)
        console.log(array);
    }
}

function showMinMax() {
    console.log(`Nilai tertinggi adalah: ${Math.max(...array)}`)
    console.log(`Nilai terendah adalah: ${Math.min(...array)}`)
}

function showAvg(ar) {
    let total = 0;
    for (let i = 0; i < arr.length; i++) {
        total += ar[i];
    }
    let avg = total / ar.length;
    console.log(`Nilai rata-ratanya adalah : ${avg}`)
}

function showPass(ar) {
    let pass = 0;
    let notPass = 0;
    for (let i = 0; i < ar.length; i++) {
        if (ar[i] >= 60) {
            pass += 1;
        } else {
            notPass += 1;
        }
    }
    console.log(`Jumlah Siswa Lulus : ${pass}, Tidak Lulus: ${notPass}`)
}

function showHigh(ar) {
    let high = [];
    for (let i = 0; i < ar.length; i++) {
        if (arr[i] == 90 || ar[i] == 100) {
            high.push(ar[i]);
        }
    }
    console.log(`Nilai tinggi :  ${high.join()}`)
}

function showSorted(ar) {
    let sortedArr = ar.sort(function(a, b) { return (a - b) })

    console.log(`Urutan nilai dari terendah ke tertinggi : ${sortedArr.join(", ")}`)
}
async function inAr() {
    let inn = await input("Masukkan Nilai : ");
    while (inn != "q" && Number.isInteger(+inn) == true) {
        pushArray(+inn);

        inn = await input("Masukkan Nilai : ");
        if (inn != "q" && Number.isInteger(+inn) == true) {
            inAr();
        } else if (inn == "q") {
            console.clear()
            console.log("Input selesai, berikut adalah nilai yang telah anda masukkan: \n", array);
        } else {
            console.clear()
            console.log(`Nilai yang anda masukkan harus berupa angka, anda memasukkan "${inn}"`);
            inAr();
        }
    }
    if (inn == "q") {
        console.clear()
        console.log("Input selesai, berikut adalah nilai yang telah anda masukkan: \n", array);
        showResult();
    } else {
        console.clear()
        console.log(`Nilai yang anda masukkan harus berupa angka, anda memasukkan  "${inn}"`);
        inAr();

    }
}
inAr();